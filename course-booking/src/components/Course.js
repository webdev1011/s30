import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

export default function Course({ course }) {
  console.log(course);
  //destructure the course prop into its properties  //const {name, description, price} = course;
  if (course) {
    return (
      <Card>
        <Card.Body>
          <Card.Title>{course.name}</Card.Title>
          <Card.Text>
            <span className="subtitle">Description:</span>
            <br />
            {course.description}
            <br />
            <span className="subtitle">Price: </span>
            PhP{course.price}
            <br />
          </Card.Text>
          <Button variant="primary" disabled>
            Enroll
          </Button>
        </Card.Body>
      </Card>
    );
  } else {
    return "";
  }
}
