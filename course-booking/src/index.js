import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import Navbar from './components/Navbar';
import './index.css';

import Home from './pages/Home';
import Course from './pages/Course';

//import the bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'

//render the component on the root div
ReactDOM.render(
	<Fragment>
		<Navbar />
		<Home />
		<Course />
	</Fragment>,
   document.getElementById('root')
);


