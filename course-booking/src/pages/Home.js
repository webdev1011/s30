import React, { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Course from '../components/Course';

export default function Home() {
	return (
		<Fragment>
			<Container>
				<Banner />
				<Highlights />
				<Course />
			</Container>
		</Fragment>
	)
}