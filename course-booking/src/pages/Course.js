import React, { Fragment } from "react";
import Course from "../components/Course";

import courses from "../data/courses";

export default function Courses() {
  //create multiple course components to the content of coursesData
  const CourseCards = courses.map((course) => {
    return <Course key={course.id} course={course} />;
  });
  return <Fragment>{CourseCards}</Fragment>;
}
